package nl.yvarbergman.salarycalculator.service;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SalaryServiceTest {

    SalaryService service = new SalaryService();

    @Test
    void calculateSalary() {
        BigDecimal expected = BigDecimal.valueOf(5364.25);
        expected = expected.setScale(2);
        assertEquals(expected, service.calculateSalary(40, 168, 65));
    }

    @Test
    void determineBaseSalary40() {
        BigDecimal expected = BigDecimal.valueOf(2250);
        expected = expected.setScale(2);
        assertEquals(expected, service.determineBaseSalary(40));
    }

    @Test
    void determineBaseSalary36() {
        BigDecimal expected = BigDecimal.valueOf(2025);
        expected = expected.setScale(2);
        assertEquals(expected, service.determineBaseSalary(36));
    }

    @Test
    void determineBaseSalary44() {
        BigDecimal expected = BigDecimal.valueOf(2475.0);
        expected = expected.setScale(2);
        assertEquals(expected, service.determineBaseSalary(44));
    }

    @Test
    void determineRestTurnover() {
        BigDecimal expected = BigDecimal.valueOf(3475);
        expected = expected.setScale(2);
        assertEquals(expected, service.determineRestTurnover(BigDecimal.valueOf(10000), 40));
    }

    @Test
    void determineRestTurnoverEven() {
        BigDecimal expected = BigDecimal.ZERO;
        expected = expected.setScale(2);
        assertEquals(expected, service.determineRestTurnover(BigDecimal.valueOf(6525), 40));
    }

    @Test
    void determineRestTurnoverZero() {
        BigDecimal expected = BigDecimal.ZERO;
        expected = expected.setScale(2);
        assertEquals(expected, service.determineRestTurnover(BigDecimal.ZERO, 40));
    }

    @Test
    void determineAdditions() {
        BigDecimal expected = BigDecimal.valueOf(77.50);
        expected = expected.setScale(2);
        assertEquals(expected, service.determineAdditions(36));
    }

    @Test
    void determineVacationMoney() {
        BigDecimal expected = BigDecimal.valueOf(80.00);
        expected = expected.setScale(2);
        assertEquals(expected, service.determineVacationMoney(BigDecimal.valueOf(1000)));
    }

    @Test
    void determineRatioContractHoursUnder1() {
        BigDecimal expected = BigDecimal.valueOf(0.90);
        expected = expected.setScale(2);
        assertEquals(expected, service.determineRatioContractHours(36));
    }

    @Test
    void determineRatioContractHours1() {
        BigDecimal expected = BigDecimal.valueOf(1);
        expected = expected.setScale(2);
        assertEquals(expected, service.determineRatioContractHours(40));
    }

    @Test
    void determineRatioContractHoursOver1() {
        BigDecimal expected = BigDecimal.valueOf(1.10);
        expected = expected.setScale(2);
        assertEquals(expected, service.determineRatioContractHours(44));
    }
}
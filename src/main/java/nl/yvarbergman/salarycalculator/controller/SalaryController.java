package nl.yvarbergman.salarycalculator.controller;

import nl.yvarbergman.salarycalculator.service.SalaryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class SalaryController {

    SalaryService service = new SalaryService();

    @GetMapping(path = "/salary")
    public BigDecimal getSalary(@RequestParam int contractHours , @RequestParam int hours, @RequestParam int tariff) {
        return service.calculateSalary(contractHours, hours, tariff);
    }
}

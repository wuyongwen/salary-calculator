package nl.yvarbergman.salarycalculator.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class SalaryService {
    private static final BigDecimal COMMISSION_THRESHOLD = BigDecimal.valueOf(6525);

    public BigDecimal calculateSalary(int contractHours, int declaredHours, int chargedTariff) {
        BigDecimal hours = BigDecimal.valueOf(declaredHours);
        BigDecimal tariff = BigDecimal.valueOf(chargedTariff);

        BigDecimal revenue = hours.multiply(tariff);

        BigDecimal baseSalary = determineBaseSalary(contractHours);
        BigDecimal additions = determineAdditions(contractHours);
        BigDecimal restTurnover = determineRestTurnover(revenue, contractHours);

        BigDecimal vacationMoney = determineVacationMoney(baseSalary);
        BigDecimal provision = restTurnover.multiply(BigDecimal.valueOf(0.65));

        BigDecimal salary = baseSalary.add(provision).add(vacationMoney).add(additions);
        salary = salary.setScale(2, RoundingMode.HALF_UP);
        return salary;
    }

    public BigDecimal determineBaseSalary(int contractHours) {
        BigDecimal baseSalary = BigDecimal.valueOf(2250).multiply(determineRatioContractHours(contractHours));
        baseSalary = baseSalary.setScale(2, RoundingMode.HALF_UP);
        return baseSalary;
    }

    public BigDecimal determineRestTurnover(BigDecimal revenue, int contractHours) {
        BigDecimal restTurnover = revenue.subtract(COMMISSION_THRESHOLD
                .multiply(determineRatioContractHours(contractHours)));

        if (restTurnover.compareTo(BigDecimal.ZERO) < 0) {
            restTurnover = BigDecimal.ZERO;
        }

        restTurnover = restTurnover.setScale(2, RoundingMode.HALF_UP);
        return restTurnover;
    }

    public BigDecimal determineAdditions(int contractHours) {
        BigDecimal internetCompensation = BigDecimal.valueOf(27.5);
        BigDecimal carwashCompensation = BigDecimal.valueOf(10);
        BigDecimal lunchCompensation = BigDecimal.valueOf(12.5);
        BigDecimal phoneCompensation = BigDecimal.valueOf(27.5);

        BigDecimal additions = internetCompensation
                .add(carwashCompensation)
                .add(lunchCompensation)
                .add(phoneCompensation);
        additions = additions.setScale(2, RoundingMode.HALF_UP);
        return additions;
    }

    public BigDecimal determineVacationMoney(BigDecimal baseSalary) {
        BigDecimal vacationMoney = baseSalary.multiply(BigDecimal.valueOf(0.08), MathContext.UNLIMITED);
        vacationMoney = vacationMoney.setScale(2, RoundingMode.HALF_UP);
        return vacationMoney;
    }

    public BigDecimal determineRatioContractHours(int contractHours) {
        BigDecimal ratio = BigDecimal.valueOf(contractHours).divide(BigDecimal.valueOf(40));
        ratio = ratio.setScale(2, RoundingMode.HALF_UP);
        return ratio;
    }
}
